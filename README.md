# Moved to GitHub
This project page is obsolete. Please visit [okerr-dev on GitHub](https://github.com/yaroslaff/okerr-dev).

# Status of this repo
This is DEVELOPMENT public git repository for server-side of okerr project (and there is no production yet). 
Sometimes code in this repo could be unstable, untested, and 'work in progress'. 
But versions with git tags (`git tag -l`) are usually stable.

If you're just using [okerr.com](https://okerr.com/) monitoring service and looking for client-side software, you need 
[okerrupdate](https://gitlab.com/yaroslaff/okerrupdate) or [okerrclient](https://gitlab.com/yaroslaff/okerrclient).

# Installation
Brief:
```shell
./okerr-install.py --local --email USER@EMAIL.COM
```

But better read wiki page for [Installation](https://gitlab.com/yaroslaff/okerr-dev/-/wikis/Install).

# WIKI
Main docs is in wiki:
https://gitlab.com/yaroslaff/okerr-dev/-/wikis/

# Other okerr resources
- [Okerr main website](https://okerr.com/)
- [Okerr-server source code repository](gitlab.com/yaroslaff/okerr-dev/) and [okerr server wiki doc](https://gitlab.com/yaroslaff/okerr-dev/wikis/)
- [Okerr client (okerrupdate) repositoty](https://gitlab.com/yaroslaff/okerrupdate) and [okerrupdate wiki doc](https://gitlab.com/yaroslaff/okerrupdate/wikis/)
- [Okerrbench network server benchmark](https://gitlab.com/yaroslaff/okerrbench)
- [Okerr custom status page](https://gitlab.com/yaroslaff/okerr-status)
- [Okerr JS-powered static status page](https://gitlab.com/yaroslaff/okerrstatusjs)
- [Okerr network sensor](https://gitlab.com/yaroslaff/sensor)
